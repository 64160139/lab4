import java.util.Scanner;
public class Array9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int [] arr = new int [3];
        for(int i = 0 ; i < arr.length ; i++){
            System.out.print("Please input arr["+i+"]: ");
            arr[i] = sc.nextInt();
             
        }
        System.out.print("arr = ");
        for(int j =0 ;j<arr.length ; j++){
            System.out.print(arr[j]+" ");
        }
        System.out.print("please input search value: ");
        int fine = sc.nextInt();
        int index = -1;
        for(int i = 0 ; i < arr.length ; i++){
            if(arr[i] == fine){
                index = i;
                break ; 
            }
        }
        if(index>=0){
            System.out.print("found at index: "+index);
        }
        else{
            System.out.println("not found");
        }
        
    }
}
