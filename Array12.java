import java.util.Scanner;
public class Array12{
    static void welcome(){
        System.out.println("Welcome to my app!!!");
    }
    static void menu(){
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");

    }
    static int inputchoice(){
        int choice ;
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input your choice(1-3):");
            choice = sc.nextInt();
            if(choice>=1 && choice <=3){
                return choice;
            }
            else{
                System.out.println("Error: Please input between 1-3");
            }
        }
    }
    static  void  hello(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time:");
        int hello1 = sc.nextInt();
        for (int i = 0; i < hello1; i++) {
            System.out.println("Hello World!!!");
        }
        return;
    }
    static void result(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number:");
        int num1 = sc.nextInt();
        System.out.print("Please input second number:");
        int num2 = sc.nextInt();
        System.out.println("Result = "+(num1+num2));
        return ;
    }
    static void exit(){
        System.out.println("Bye!!");
        System.exit(0);
        
 
    }
    public static void main(String[] args) {
        while (true) {
            int choice =0;
            welcome();
            menu();
            choice = inputchoice();
            switch (choice) {
                case 1:
                    hello(); 
                    break;
                case 2:
                    result();
                    break;
                case 3:
                    exit();
                    break;
                    
            }

        }
        
    }
    
}